import React from 'react';
import axios from 'axios';

interface IProps{
    title: string;
}

interface IState {
    result: string
}

export default class Section1 extends React.Component<IProps, IState>{
    public state: IState = {
        result: ''
    }

    public componentDidMount(){
        axios.get('/myvincent/vincent/api/v1/mock/GetData', {
            params: {
                key: 'mydata'
            }
          })
          .then((response: any) => {
              console.log(response);
              this.setState({result: response.data});
          });

          console.log(this.state.result);
    }

    public render(){
        return (
            <div>
                <h1>{this.props.title}</h1>
                Section 1 region
                <p>{this.state.result}</p>
            </div>
        )
    }
}