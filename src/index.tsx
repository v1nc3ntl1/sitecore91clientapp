import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Section1 from './Section1';
import * as serviceWorker from './serviceWorker';
import Navigation from './Navigation';

const root = document.getElementById('root');

if (root){
    ReactDOM.render(<App />, root);
}

const main = document.getElementById('divMain');

if (main){
    ReactDOM.render(<Section1 title="Section 1" />, main);
}

const nav = document.getElementById('divNavigation');

if (nav){
    ReactDOM.render(<Navigation />, nav);
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
