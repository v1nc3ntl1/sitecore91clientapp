import React from 'react';
import axios from 'axios';

interface INavigationProps {}

interface INavigationState {
    menuItem: any[]
}

export default class Navigation extends React.Component<INavigationProps, INavigationState>{

    public state: INavigationState = {
        menuItem: []
    }

    public componentDidMount(){
        axios.get('/myvincent/vincent/api/v1/navigation/get')
          .then((response: any) => {
              this.setState({menuItem: response.data})
          });

          console.log(this.state.menuItem);
    }

    public renderMenu = () => {
        const menu = this.state.menuItem.map(mi => (
            <li><a href={mi.Url}>{mi.Name}</a></li>
        ));

        return <>{menu}</>;
    }

    public render(){
        return (
            <nav>
                <div className="navbar-wrapper">
                    <a href="/">Basic Navbar</a>
                </div>
              <ul>
                {this.renderMenu()}
              </ul>
            </nav>
        );
    }
}